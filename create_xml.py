import csv
import sys
import xml.etree.ElementTree as ET
from lxml import etree
from xml.dom import minidom
from resources.adder import add_row_to_xml

# Create root of XML structure for output document
document = ET.Element('ips')
document.set('xsi:schemaLocation', 
'http://upu.int/ipsexternalrecptcl IPS.ApplicationLayer.ImportExport.ExternalReceptacle.Import.xsd')
document.set('xmlns', 'http://upu.int/ipsexternalrecptcl')
document.set('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')

# Read input files set as system arguments
for input_file in sys.argv[1:]:
  print('Reading input file: ' + input_file)
  manifest_file = open(input_file, mode='r', encoding="utf-8-sig")
  reader = csv.reader(manifest_file, delimiter=';', quotechar='"')
  
  if '_' in sys.argv[1]:
    pl_number = sys.argv[1].split('_')[0]
  else:
    pl_number = sys.argv[1].split('.')[0]
  output_file = pl_number + ".xml"

  # Add rows to XML tree data structure
  count = 0
  for row in reader:
    if len(row) != 36:
      print('Skipping row: ' + str(row))
      continue
    add_row_to_xml(document, row)
    count += 1
  print("From {input_file} {count} records were added to the XML document.".format(input_file=input_file, count=count))

# Transfor data structure to text data
et = ET.ElementTree(document)
xmlstr = minidom.parseString(ET.tostring(document, encoding="unicode", xml_declaration=True)).toprettyxml(indent="   ")

# Create output XML file
with open(output_file, "w") as f:
    f.write(xmlstr)

manifest_file.close()

# Validate if output file is valid against XML schema
xml_output_file = etree.parse(output_file)
xml_validator = etree.XMLSchema(file="resources/IPS.ApplicationLayer.ImportExport.ExternalReceptacle.Import.xsd")
is_valid = xml_validator.validate(xml_output_file)
log = xml_validator.error_log

# Output messege
if is_valid:
  print('XML file {} is successfully created and it is valid.'.format(output_file))
else:
  print('ERROR: created XML is not valid for Post Office. Contact IT and send error log bellow:')
  print(log)
