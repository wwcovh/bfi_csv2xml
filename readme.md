#  Create XML for post office

This script creates XML file from input CSV manifest(s).

## Installation

1. Install [Python 3 for Windows](https://www.python.org/downloads/), don't forget to add Python to PATH (the installer prompts for this at the end of installation)
2. Download and unzip [Git for Windows](https://git-scm.com/download/win) to ``C:\Tools`` (so the path to the binary is ``C:\Tools\Git\bin\git.exe``)
3. ``git clone`` the repository into ``C:\Tools``
4. Install dependencies:
    - ``pip install lxml transliterate``

## Input files

Input files have to be in CSV format.  
Input files have to use semicolon (;) as a separator.  
Input files have to be without header row.  
Input files have to be in the same directory as create_xml.py file.  
Input files should be encoded in UTF-8.

## Running the script

For run the script use command line interface:  
`create_xml.py file_1.csv` for one file  
or  
`create_xml.py file_1.csv file_2.csv file_3.csv` for multiple files.

Script creates one output XML document file. 

After successfull run  
`XML file document.xml is successfully created and it is valid.` 
messege appears.

## Environment

Before first run you should test the script on you machine by command
`create_xml.py test.csv`
and it should successfully creates `text.xml` file.

## Troubleshooting

If any error appers contact Radek Dobrovoln� (radek.dobrovolny@gmail.com) or your IT.