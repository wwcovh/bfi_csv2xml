import xml.etree.ElementTree as ET
from datetime import datetime
from resources.resources import office_codes

def add_row_to_xml(document, row):
  for i in range(len(row)):
    row[i] = row[i].strip()

  if row[0] == 'PR': # Puerto Rico solves US post office
    row[0] = 'US'

  external_receptacle = None
  for node in document:
    if node.attrib['Id'] == row[15]:
      external_receptacle = node
  if not external_receptacle:
    external_receptacle = ET.SubElement(document, 'ExternalRecptcl')
    external_receptacle.set('Id', row[15])

  if external_receptacle.findall('DestinationCountry') == []:
    dest_country = ET.SubElement(external_receptacle, 'DestinationCountry')
    dest_country.text = row[0]

  if external_receptacle.findall('DestinationOffice') == []:
    dest_office_cd = ET.SubElement(external_receptacle, 'DestinationOffice')
    dest_office_cd.text = row[0] + office_codes.get(row[0], "")

  if external_receptacle.findall('MailCategory') == []:
    mail_category = ET.SubElement(external_receptacle, 'MailCategory')
    mail_category.text = 'A'

  if external_receptacle.findall('ReceptacleClass') == []:
    receptacle_class = ET.SubElement(external_receptacle, 'ReceptacleClass')
    receptacle_class.text = 'U'

  if row[0] == 'US':
    subclass_text = 'UB'
  elif row[0] in ('UA', 'UY', 'MT', 'CL', 'AR', 'CO'):
    subclass_text = 'UL'
  else:
    subclass_text = 'UN'
  if external_receptacle.findall('ReceptacleSubclass') == []:
    receptacle_subclass = ET.SubElement(external_receptacle, 'ReceptacleSubclass')
    receptacle_subclass.text = subclass_text

  if external_receptacle.findall('Weight') == []:
    receptacle_weight = ET.SubElement(external_receptacle, 'Weight')
    receptacle_weight.text = row[34].replace(',','.')
  else:
    receptacle_weight = external_receptacle.find('Weight')
    receptacle_weight.text = str(round(float(receptacle_weight.text) + float(row[34].replace(',','.')), 3))

  if external_receptacle.findall('SubType') == []:
    receptacle_subtype = ET.SubElement(external_receptacle, 'SubType')
    receptacle_subtype.text = 'BG'

  if external_receptacle.findall('ContentFormat') == []:
    receptacle_content_format = ET.SubElement(external_receptacle, 'ContentFormat')
    receptacle_content_format.text = 'E'

  if external_receptacle.findall('Exempt') == []:
    receptacle_exempt = ET.SubElement(external_receptacle, 'Exempt')
    receptacle_exempt.text = 'false'

  if external_receptacle.findall('Prepared') == []:    
    prepared = ET.SubElement(external_receptacle, 'Prepared')
    if row[3] != '':
      prepared_obj = datetime.strptime(row[3], '%Y/%m/%d')
    else:
      prepared_obj = datetime.now().replace(microsecond=0)
    prepared.text = str(prepared_obj.isoformat()) + 'Z'

  if external_receptacle.findall('SendingCustomer') == []:
    sending_customer = ET.SubElement(external_receptacle, 'SendingCustomer')
    sending_customer.text = 'BEF'
    
  if external_receptacle.findall('SendingCustomerNumber') == []:
    sending_customer_number = ET.SubElement(external_receptacle, 'SendingCustomerNumber')
    sending_customer_number.text = '2022PL130'

  mail_items = external_receptacle.find('MailItems')
  if not mail_items:
    mail_items = ET.SubElement(external_receptacle, 'MailItems')

  mail_item = ET.SubElement(mail_items, 'MailItem')
  mail_item.set('ItemId', row[5])

  item_weight = ET.SubElement(mail_item, 'ItemWeight')
  item_weight.text = row[34].replace(',','.')

  dutiable_ind = ET.SubElement(mail_item, 'DutiableInd')
  dutiable_ind.text = 'D'

  class_cd = ET.SubElement(mail_item, 'ClassCd')
  class_cd.text = row[4]

  content = ET.SubElement(mail_item, 'Content')
  content.text = 'M'

  orig_country_cd = ET.SubElement(mail_item, 'OrigCountryCd')
  orig_country_cd.text = 'CZ'

  dest_country_cd = ET.SubElement(mail_item, 'DestCountryCd')
  dest_country_cd.text = row[0]

  postal_status_fcd = ET.SubElement(mail_item, 'PostalStatusFCd')
  postal_status_fcd.text = 'MINL'

  inserted_into = ET.SubElement(mail_item, 'InsertedIntoExternalReceptacle')  

  posting_date = ET.SubElement(inserted_into, 'Date')
  if row[3] != '':    
    posting_date_obj = datetime.strptime(row[3], '%Y/%m/%d')
  else:
    posting_date_obj = datetime.now().replace(microsecond=0)
  posting_date.text = str(posting_date_obj.isoformat()) + 'Z'

  office_cd = ET.SubElement(inserted_into, 'OfficeCd')
  office_cd.text = 'CZPRGA'
